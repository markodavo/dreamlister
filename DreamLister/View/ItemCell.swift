//
//  ItemCell.swift
//  DreamLister
//
//  Created by Mark Davidson on 12/9/17.
//  Copyright © 2017 Mark Davidson. All rights reserved.
//

import UIKit

class ItemCell: UITableViewCell {

    @IBOutlet weak var thumb: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var details: UILabel!
    
    func configureCell(Item: Item) {
        
        title.text = Item.title;
        price.text = "\(Item.price)"
        details.text = Item.details;
        thumb.image = Item.toImage?.image as? UIImage;
        
        
        
        
        
    }
    
    
}
