//
//  Item+CoreDataClass.swift
//  DreamLister
//
//  Created by Mark Davidson on 11/9/17.
//  Copyright © 2017 Mark Davidson. All rights reserved.
//
//

import Foundation
import CoreData


public class Item: NSManagedObject {

    public override func awakeFromInsert() {
        //Date and time stamp when created at the time it is inserted. 
        super.awakeFromInsert()
        
        self.created = NSDate()
    }
    
}
